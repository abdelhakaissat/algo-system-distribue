package main

import (
	"fmt"
)

func main() {

	//declare tab of channels
	var tabChan [3]chan bool

	// sync within the main!
	sync := make(chan bool)

	// instanciate channel table
	for i := range tabChan {
		tabChan[i] = make(chan bool)
	}

	//launch users
	for i := range tabChan {
		go func(index int) {
			user(tabChan[index], index+1)
			sync <- true
		}(i)
	}

	//TODO launch controller
	go func() {
		controller(tabChan)
		sync <- true
	}()

	// wait for all the go routines to end
	for i := 0; i < 4; i++ {
		<-sync
	}

	<-sync
}
func user(cha chan bool, ent int) {
	for {
		cha <- true
		//section critique!
		fmt.Println("im the kind of the world! (and the one who is inside!)")
		fmt.Println("And my number is: ", ent)

		cha <- true

	}
}

func controller(channels [3]chan bool) {
	for {
		select {
		case <-channels[0]:
			<-channels[0]

		case <-channels[1]:
			<-channels[2]

		case <-channels[2]:
			<-channels[2]
		}

	}

}

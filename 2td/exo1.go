package main

import (
	"fmt"
)

func main() {

	//declare tab of channels
	var tabChan [3]chan int

	// sync within the main!
	sync := make(chan bool)

	// instanciate channel table
	for i := range tabChan {
		tabChan[i] = make(chan int)
	}

	//launch users
	for i := range tabChan {
		go func(index int) {
			user(tabChan[index], index+1)
			sync <- true
		}(i)
	}

	//TODO launch controller
	go func() {
		controller(tabChan)
		sync <- true
	}()

	// wait for all the go routines to end
	for i := 0; i < 4; i++ {
		<-sync
	}

}

func controller(channels [3]chan int) {
	//
	var maxi = -1
	var current int

	// read from all the users
	for i := range channels {
		current = <-channels[i]
		if current > maxi {
			maxi = current
		}
	}

	//send the maximum to all the users so the one who is the maximim
	// can actually say it!
	for i := range channels {
		channels[i] <- maxi
	}

}

func user(chanComm chan int, id int) {
	//send it to the channel!
	var result int
	chanComm <- id
	// receive the respones
	result = <-chanComm
	if result == id {
		fmt.Println("im ", id, " the winner")
	}

}

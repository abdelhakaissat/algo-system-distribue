//1.1.1: Calcul du maximum des identifiants!

func node(in chan int, out chan, racine bool){
  /*
  */
	//declarations des variables!	
	var messIn [] int
	var messOut [] int
	var parent int
	var visite bool
	messIn = make([] int, len(in))
	messOut = make([] int, len(out))
	visite = racine

    // NOTE: reponse commence ici:
    var maxId int = id
      

	// initialiser les sorties!
	for i := range out {
			messOut[i] = null
	}
    if !racine{
        messOut[parent] = maxId
    }

	//boucle de synchronisation
	for nbTours = 0; nbTours < diametre; nbTours++ { 
	    communication(in, messIn, out, messOut)	

		// boucle principale 1.1.1
	    for i := range in{
	    	if messIn[i]  != null{
	       		if messIn[i] > maxId{
	       			maxId = messIn[i]
	       		}
	    	}
	    }

	    // on change par cette boucle pour repondre a la question 1.1.2 car on aura tabFils
	    /*
	    	for _,i range tabFils {
	    		if messIn[i] > maxId{
	    			maxId = messIn[i]
	    		}
	    	}
	    */

	    //envoie du plus grand id au pere!
	    if !racine{
	    	messOut[parent]= maxId
	    }
  	}

}
// Part 1.2

func node(in chan int, out chan, racine bool){
  /*
  */
	//declarations des variables!	
	var messIn [] int
	var messOut [] int
	var parent int
	var visite bool
	messIn = make([] int, len(in))
	messOut = make([] int, len(out))
	visite = racine

    // NOTE: reponse commence ici:
    var distMax int= 0 //changemnt ici
      

	// initialiser les sorties!
	for i := range out {
			messOut[i] = null
	}
    if !racine{
        messOut[parent] = distMax
    }

	//boucle de synchronisation
	for nbTours = 0; nbTours < diametre; nbTours++ { 
	    communication(in, messIn, out, messOut)	

		// boucle principale 1.1.1
	    for i := range in{
	    	if messIn[i]  != null{
	       		if messIn[i] > distMax{
	       			distMax = messIn[i]
	       		}
	    	}
	    }

	    // on change par cette boucle pour repondre a la question 1.1.2 car on aura tabFils
	    /*
	    	for _,i range tabFils {
	    		if messIn[i] > maxId{
	    			maxId = messIn[i]
	    		}
	    	}
	    */

	    //envoie du plus grand id au pere!
	    if !racine{
	    	messOut[parent]= distMax + 1 
	    }
  	}

}

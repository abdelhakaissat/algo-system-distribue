package main

import "fmt"

func main() {
	c := make(chan int)
	d := make(chan int)
	s1 := make(chan bool)
	s2 := make(chan bool)

	go A(c, d, 6, s1)
	go A(d, c, 7, s2)
	<-s1
	<-s2
	fmt.Println("-------------")
}

func A(c1, c2 chan int, init int, synch chan bool) {
	var x1, x2 int
	x2 = init
	s := make(chan bool)

	go func() {
		x1 = <-c1
		s <- true
	}()
	c2 <- x2

	// insure syncronization within the function
	<-s
	// disllay value sent via channel
	fmt.Println(x1)

	// insure syncronization with the main
	synch <- true
}
